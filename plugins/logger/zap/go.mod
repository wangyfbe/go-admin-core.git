module gitee.com/wangyfbe/go-admin-core/plugins/logger/zap

go 1.14

require (
	gitee.com/wangyfbe/go-admin-core v1.10.0
	go.uber.org/zap v1.10.0
)

replace gitee.com/wangyfbe/go-admin-core => ../../../
