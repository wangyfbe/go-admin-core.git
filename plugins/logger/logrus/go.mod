module gitee.com/wangyfbe/go-admin-core/plugins/logger/logrus

go 1.14

require (
	gitee.com/wangyfbe/go-admin-core v1.10.0
	github.com/sirupsen/logrus v1.8.0
)

replace gitee.com/wangyfbe/go-admin-core => ../../../
