package config

type Cos struct {
	Url       string
	SecretId  string
	SecretKey string
}

var CosConfig = new(Cos)
